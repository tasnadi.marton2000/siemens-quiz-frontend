import React from "react";
import Menu from "../components/Menu";

function HomePage({ score }) {
  return (
    <div>
      <Menu />
      <ScoreComponent score={score} />
    </div>
  );
}

export default HomePage;
