import React from "react";
import Menu from "../components/Menu";
import Main from "../components/Main";
import ColoredCircles from "../components/ColoredCircles";
import QuizGame from "../components/QuizGame";

function QuestionPage() {
  return (
    <div>
      <Menu />
      <QuizGame />
    </div>
  );
}

export default QuestionPage;
