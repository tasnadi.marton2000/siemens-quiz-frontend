import React from "react";
import Menu from "../components/Menu";
import Main from "../components/Main";
import ColoredCircles from "../components/ColoredCircles";

function HomePage() {
  return (
    <div>
      <Menu />
      <Main />
    </div>
  );
}

export default HomePage;
