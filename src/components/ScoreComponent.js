import React from "react";
import ColoredCircles from "./ColoredCircles";
import "../App.css";

function ScoreComponent({ score }) {
  return (
    <div className="mainContainer">
      <ColoredCircles />
      <div className="title">YOUR SCORE: {score}</div>
    </div>
  );
}

export default ScoreComponent;
