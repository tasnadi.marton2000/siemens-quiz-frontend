import React from "react";

function ColoredCircles() {
  const colors = [
    "#FF5733",
    "#33FF57",
    "#5733FF",
    "#FF33E9",
    "#33D1FF",
    "#FFE933",
  ];

  return (
    <div
      style={{ display: "flex", justifyContent: "center", marginTop: "50px" }}
    >
      {colors.map((color, index) => (
        <div
          key={index}
          style={{
            width: "25px",
            height: "25px",
            borderRadius: "50%",
            backgroundColor: color,
            margin: "0 5px",
          }}
        ></div>
      ))}
    </div>
  );
}

export default ColoredCircles;
