import { Button } from "@mui/material";
import { Link } from "react-router-dom";
import "../App.css";

function MenuButtons(props) {
  return (
    <Button
      className="menuButton"
      component={Link}
      to={props.uri}
      color="inherit"
      sx={{ marginLeft: "10px" }}
    >
      {props.text}
    </Button>
  );
}

export default MenuButtons;
