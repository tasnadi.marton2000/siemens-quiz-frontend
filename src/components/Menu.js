import { AppBar, Toolbar } from "@mui/material";
import MenuButtons from "./MenuButtons";

function Menu() {
  return (
    <AppBar>
      <Toolbar className="menuBar">
        <>
          <MenuButtons uri="/" text="Home" />
        </>
      </Toolbar>
    </AppBar>
  );
}

export default Menu;
