import React from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import ColoredCircles from "./ColoredCircles";
import "../App.css";

function Main() {
  const navigate = useNavigate();

  const handleOnClickPlayButton = async () => {
    try {
      await axios.post("http://localhost:8082/game");
      navigate("/game");
    } catch (error) {
      console.error("Error:", error);
    }
  };

  return (
    <div className="mainContainer">
      <ColoredCircles />
      <div className="title">QUIZ</div>
      <button className="playButton" onClick={handleOnClickPlayButton}>
        PLAY!
      </button>
    </div>
  );
}

export default Main;
