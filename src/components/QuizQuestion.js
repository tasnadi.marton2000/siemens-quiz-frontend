import React from "react";
import "../App.css";

function QuizQuestion({
  question,
  options,
  onSelectOption,
  selectedOption,
  correctAnswer,
}) {
  const optionLetters = ["A", "B", "C", "D"];

  function defineClassName(option) {
    if (correctAnswer === selectedOption && selectedOption === option) {
      return "correctOptionButton";
    } else if (
      selectedOption === option &&
      correctAnswer !== null &&
      correctAnswer !== option
    ) {
      return "incorrectOptionButton";
    } else if (selectedOption !== option && correctAnswer === option) {
      return "correctOptionButton";
    } else if (correctAnswer === null && selectedOption === option) {
      return "selectedOptionButton";
    } else {
      return "optionButtonStyle";
    }
  }

  return (
    <div className="quiz-question-container">
      <p className="question">{question}</p>
      <div className="options-container">
        {options.map((option, index) => (
          <button
            key={index}
            className={defineClassName(option)}
            onClick={() => onSelectOption(option)}
          >{`${optionLetters[index]}. ${option}`}</button>
        ))}
      </div>
    </div>
  );
}

export default QuizQuestion;
