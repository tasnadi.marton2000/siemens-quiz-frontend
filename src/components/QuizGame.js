import React, { useState, useEffect } from "react";
import axios from "axios";
import QuizQuestion from "./QuizQuestion";
import ColoredCircles from "./ColoredCircles";
import ScoreComponent from "./ScoreComponent";
import "../App.css";

function QuizGame() {
  const [selectedOption, setSelectedOption] = useState(null);
  const [checked, setChecked] = useState(false);
  const [question, setQuestion] = useState(null);
  const [options, setOptions] = useState(null);
  const [correctAnswer, setCorrectAnswer] = useState(null);
  const [loading, setLoading] = useState(true);
  const [score, setScore] = useState(0);
  const [finished, setFinished] = useState(false);

  const handleSelectOption = (option) => {
    setSelectedOption(option);
  };

  const handleChecked = (isChecked) => {
    setChecked(isChecked);
    if (correctAnswer === selectedOption) {
      let newScore = score + 1;
      setScore(newScore);
    }
  };

  const getRandomQuestion = async () => {
    try {
      setLoading(true);
      const response = await axios.get("http://localhost:8082/randomQuestion");
      setSelectedOption(null);
      const data = response.data;
      setQuestion(data.question);
      let list = [];
      list.push(data.optionA);
      list.push(data.optionB);
      list.push(data.optionC);
      list.push(data.optionD);
      setOptions(list);
      switch (data.correctAnswer) {
        case "A":
          setCorrectAnswer(data.optionA);
          break;
        case "B":
          setCorrectAnswer(data.optionB);
          break;
        case "C":
          setCorrectAnswer(data.optionC);
          break;
        case "D":
          setCorrectAnswer(data.optionD);
          break;
        default:
          setCorrectAnswer(null);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      if (error.response.status === 415) {
        setQuestion(-1);
        setFinished(true);
      }
    } finally {
      setLoading(false);
      setChecked(false);
    }
  };

  useEffect(() => {
    getRandomQuestion();
  }, []);

  return (
    <div className="mainContainer">
      {!loading && (
        <div>
          <ColoredCircles />
          {question !== -1 ? (
            <QuizQuestion
              question={question}
              options={options}
              onSelectOption={handleSelectOption}
              selectedOption={selectedOption}
              correctAnswer={checked === true ? correctAnswer : null}
            />
          ) : (
            <ScoreComponent score={score} />
          )}
          {!finished && selectedOption && !checked && (
            <button className="goButton" onClick={() => handleChecked(true)}>
              Check
            </button>
          )}
          {!finished && checked && (
            <button className="goButton" onClick={() => getRandomQuestion()}>
              Next
            </button>
          )}
        </div>
      )}
    </div>
  );
}

export default QuizGame;
