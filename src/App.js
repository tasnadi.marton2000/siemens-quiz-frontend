import "./App.css";
import Home from "./pages/HomePage";
import { Routes, Route } from "react-router-dom";
import QuestionPage from "./pages/QuestionPage";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/game" element={<QuestionPage />} />
    </Routes>
  );
}

export default App;
